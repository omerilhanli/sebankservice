package test;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import entity.Hesap;
import entity.HesapHareketi;
import entity.Kart;
import entity.Musteri;
import repository.MusteriRepository;

public class Test {

    public static void main(String[] args) {

        Musteri musteri = new Musteri();        
        createMusteri(musteri);

        MusteriRepository repo = new MusteriRepository();
        repo.persist(musteri); // SUCCESS
        repo.close();
    }
    
    public static void createMusteri(Musteri musteri){
        musteri.setMusteriAdi("Omer");
        musteri.setMusteriSoyadi("İlhanlı");
        musteri.setMusteriTcNo("11122233344");
        musteri.setCinsiyet("Erkek");
        musteri.setSifre("1234");

        Kart kart = new Kart();
        kart.setKartNumarasi((long) Math.random() * (100000));

        Hesap hesap = new Hesap();
        hesap.setBankaAdi("SE Bank");
        hesap.setHesapFullAd("Omer İlhanlı");
        hesap.setHesapLimiti(1000);
        hesap.setHesapBakiye(400);
        hesap.setHesapTipi("Vadeli");
        hesap.setHesapNo((long) Math.random() * (100000));

        HesapHareketi hesapHareketi = new HesapHareketi();
        hesapHareketi.setHareketTipi(HesapHareketi.PARA_YATIRMA);
        hesapHareketi.setHareketDegeri(100);
        hesap.setHesapBakiye(hesap.getHesapBakiye() + hesapHareketi.getHareketDegeri()); // +100 eklenir
        Date today = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy hh:mm:ss"); // tarih formatı - değiştirilebilir
        String bugun = sdf.format(today);
        hesapHareketi.setHareketTarihi(bugun);

        ArrayList<HesapHareketi> hesapHareketListesi = new ArrayList<>();
        hesapHareketListesi.add(hesapHareketi);
        hesap.setHesapHareketListesi(hesapHareketListesi); /// Hesap - HesapHareketi
        hesapHareketi.setHesap(hesap); /// Hesap - HesapHareketi

        ArrayList<Hesap> hesapList = new ArrayList<>();
        hesapList.add(hesap);
        kart.setHesapList(hesapList); /// Kart - Hesap
        hesap.setKart(kart); /// Kart - Hesap

        ArrayList<Kart> kartlar = new ArrayList<>();
        kartlar.add(kart);
        musteri.setKartList(kartlar); /// Musteri - Kart
        kart.setMusteri(musteri); /// Musteri - Kart
    }
    
}
