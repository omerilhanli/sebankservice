package test;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import entity.Hesap;
import entity.HesapHareketi;
import entity.Kart;
import entity.Musteri;

public class Main {

    public static void main(String[] args) {

        ArrayList<HesapHareketi> hl = new ArrayList<>();
        HesapHareketi hh = new HesapHareketi();
        hh.setHareketTipi("1");
        HesapHareketi hh2 = new HesapHareketi();
        hh.setHareketTipi("2");
        hl.add(hh);
        hl.add(hh2);

        Gson gs = new GsonBuilder().create();
        String str = gs.toJson(hl, ArrayList.class);
        System.out.println("str : " + str);

//        Musteri musteri = new Musteri();
//        createMusteri(musteri);
//
//        String mainJSON = "{ \"source\" : "
//                + "[ "
//                + "{ \"name\" : \"john\", \"age\" : \"20\" },"
//                + "{ \"name\" : \"micheal\", \"age\" : \"24\" },"
//                + "{ \"name\" : \"sara\", \"age\" : \"22\" }"
//                + "]"
//                + "}";
//
//        JsonObject jsonKullanici = Json.createReader(new StringReader(mainJSON)).readObject();
//        JsonArray jsonArr = jsonKullanici.getJsonArray("source");
//
//        for (int i = 0; i < jsonArr.size(); i++) {
//            JsonObject childJSONObject = jsonArr.getJsonObject(i);
//            String name = childJSONObject.getString("name");
//            String age = childJSONObject.getString("age");
//            System.out.println(i + ". " + "name : " + name + " - age : " + age);
//        }
    }

    public static void createMusteri(Musteri musteri) {
        musteri.setMusteriAdi("Omer-5");
        musteri.setMusteriSoyadi("İlhanlı-5");
        musteri.setMusteriTcNo("11122233344-5");
        musteri.setCinsiyet("Erkek-5");
        musteri.setSifre("1234-5");

        Kart kart = new Kart();
        kart.setKartNumarasi((long) Math.random() * (100000));

        Hesap hesap = new Hesap();
        hesap.setBankaAdi("SE Bank-2");
        hesap.setHesapFullAd(musteri.getMusteriAdi() + " " + musteri.getMusteriSoyadi() + "-2");
        hesap.setHesapLimiti(1000);
        hesap.setHesapBakiye(400);
        hesap.setHesapTipi("Vadeli-2");
        hesap.setHesapNo((long) Math.random() * (100000));

        HesapHareketi hesapHareketi = new HesapHareketi();
        hesapHareketi.setHareketTipi(HesapHareketi.PARA_YATIRMA);
        hesapHareketi.setHareketDegeri(200);
        hesap.setHesapBakiye(hesap.getHesapBakiye() + hesapHareketi.getHareketDegeri()); // +100 eklenir
        Date today = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy hh:mm:ss"); // tarih formatı - değiştirilebilir
        String bugun = sdf.format(today);
        hesapHareketi.setHareketTarihi(bugun);

        ArrayList<HesapHareketi> hesapHareketListesi = new ArrayList<>();
        hesapHareketListesi.add(hesapHareketi);
        hesap.setHesapHareketListesi(hesapHareketListesi); /// Hesap - HesapHareketi
        hesapHareketi.setHesap(hesap); /// Hesap - HesapHareketi

        ArrayList<Hesap> hesapList = new ArrayList<>();
        hesapList.add(hesap);
        kart.setHesapList(hesapList); /// Kart - Hesap
        hesap.setKart(kart); /// Kart - Hesap

        ArrayList<Kart> kartlar = new ArrayList<>();
        kartlar.add(kart);
        musteri.setKartList(kartlar); /// Musteri - Kart
        kart.setMusteri(musteri); /// Musteri - Kart
    }

}
