package test;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import entity.Hesap;
import servicemodel.Login;

public class MainHesaplar {
    public static void main(String[] args) {
        Gson gson = new GsonBuilder().create();
        String response = "";
        try {
            String adr = "http://localhost:8080/BankaServisi/inventory/musteri/hesaplar";
            URL url = new URL(adr);
            URLConnection con = url.openConnection();
            con.setDoOutput(true);
            con.setRequestProperty("Content-Type", "application/json");

            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            StringBuilder build = new StringBuilder();
            response = in.readLine();
            ArrayList<Hesap> hesapList = gson.fromJson(response, ArrayList.class);
            System.out.println("size : " + hesapList.size());
             
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
