package test;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import entity.AllObject;
import servicemodel.Login;

public class MainGiris {

    public static void main(String[] args) {
        Gson gson = new GsonBuilder().create();
        String response = "";
        try {
            String adr = "http://localhost:8080/BankaServisi/inventory/musteri/giris";
            URL url = new URL(adr);
            URLConnection con = url.openConnection();
            con.setDoOutput(true);
            con.setRequestProperty("Content-Type", "application/json");

            BufferedWriter outwrite = new BufferedWriter(new OutputStreamWriter(con.getOutputStream()));

            Login login = new Login();
            login.setTcNo("33311122244");
            login.setSifre("12345");
            String loginString = gson.toJson(login, Login.class);
            outwrite.append(loginString);
            outwrite.flush();

            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            StringBuilder build = new StringBuilder();
             response = in.readLine();
             System.out.println("response:"+response);
             
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
