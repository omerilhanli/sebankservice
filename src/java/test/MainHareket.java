package test;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import entity.HesapHareketi;
import servicemodel.Hareket;
import servicemodel.Login;

public class MainHareket {
    public static void main(String[] args) {
        MainHareket mai = new MainHareket();
        HesapHareketi hesapHareketi = new HesapHareketi();
        hesapHareketi.setHareketTipi(HesapHareketi.PARA_CEKME);
        hesapHareketi.setHareketDegeri(35);
        
//        mai.paraCek(hesapHareketi);
//        mai.paraYatir(hesapHareketi);
//        mai.paraTransfer(hesapHareketi);
        
        mai.hesapKilitle(hesapHareketi);
        
    }
    
    public void paraCek(HesapHareketi hesapHareketi){
        Gson gson = new GsonBuilder().create();
        String response = "";
        try {
            String adr = "http://localhost:8080/BankaServisi/inventory/musteri/paraCek";
            URL url = new URL(adr);
            URLConnection con = url.openConnection();
            con.setDoOutput(true);
            con.setRequestProperty("Content-Type", "application/json");

            BufferedWriter outwrite = new BufferedWriter(new OutputStreamWriter(con.getOutputStream()));
            
            Hareket hareket = new Hareket();
            hareket.setSahipHesapNo("98550");
            hareket.setMiktar(hesapHareketi.getHareketDegeri()+"");
            hareket.setHareketTipi(HesapHareketi.PARA_CEKME);
            hareket.setAliciHesapNo("-");
            hareket.setAliciTCNo("-");
            
            String hareketStr = gson.toJson(hareket, Hareket.class);
            outwrite.append(hareketStr);
            outwrite.flush();

            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            StringBuilder build = new StringBuilder();
             response = in.readLine();
             System.out.println("response:"+response);
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void paraYatir(HesapHareketi hesapHareketi){
        Gson gson = new GsonBuilder().create();
        String response = "";
        try {
            String adr = "http://localhost:8080/BankaServisi/inventory/musteri/paraYatir";
            URL url = new URL(adr);
            URLConnection con = url.openConnection();
            con.setDoOutput(true);
            con.setRequestProperty("Content-Type", "application/json");

            BufferedWriter outwrite = new BufferedWriter(new OutputStreamWriter(con.getOutputStream()));

            
       
            
            Hareket hareket = new Hareket();
            hareket.setSahipHesapNo("98550");
            hareket.setAliciHesapNo("-");
            hareket.setAliciTCNo("-");
            hareket.setMiktar("45");            
            hareket.setHareketTipi(HesapHareketi.PARA_YATIRMA);
            

            
            String hareketStr = gson.toJson(hareket, Hareket.class);
            outwrite.append(hareketStr);
            outwrite.flush();

            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            StringBuilder build = new StringBuilder();
             response = in.readLine();
             System.out.println("response:"+response);
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void paraTransfer(HesapHareketi hesapHareketi){
        Gson gson = new GsonBuilder().create();
        String response = "";
        try {
            String adr = "http://localhost:8080/BankaServisi/inventory/musteri/paraTransfer";
            URL url = new URL(adr);
            URLConnection con = url.openConnection();
            con.setDoOutput(true);
            con.setRequestProperty("Content-Type", "application/json");

            BufferedWriter outwrite = new BufferedWriter(new OutputStreamWriter(con.getOutputStream()));

            Hareket hareket = new Hareket();
            hareket.setSahipHesapNo("98550");
            hareket.setAliciHesapNo("53853");
            hareket.setAliciTCNo("33311122244");
            hareket.setMiktar("25");            
            hareket.setHareketTipi(HesapHareketi.PARA_TRANSFERI);
          
            String hareketStr = gson.toJson(hareket, Hareket.class);
            outwrite.append(hareketStr);
            outwrite.flush();

            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            StringBuilder build = new StringBuilder();
             response = in.readLine();
             System.out.println("response:"+response);
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void hesapKilitle(HesapHareketi hesapHareketi){
        Gson gson = new GsonBuilder().create();
        String response = "";
        try {
            String adr = "http://localhost:8080/BankaServisi/inventory/musteri/hesapKilitle";
            URL url = new URL(adr);
            URLConnection con = url.openConnection();
            con.setDoOutput(true);
            con.setRequestProperty("Content-Type", "application/json");

            BufferedWriter outwrite = new BufferedWriter(new OutputStreamWriter(con.getOutputStream()));

            Hareket hareket = new Hareket();
            hareket.setSahipHesapNo("98550");
            hareket.setAliciHesapNo("-");
            hareket.setAliciTCNo("-");
            hareket.setHareketTipi(HesapHareketi.HESAP_KILIT);
            
            String hareketStr = gson.toJson(hareket, Hareket.class);
            outwrite.append(hareketStr);
            outwrite.flush();

            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            StringBuilder build = new StringBuilder();
             response = in.readLine();
             System.out.println("response:"+response);
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
}
