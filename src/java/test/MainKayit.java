package test;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import entity.AllObject;
import entity.Hesap;
import entity.HesapHareketi;
import entity.Kart;
import entity.Musteri;

public class MainKayit {

    static Musteri justMusteri = new Musteri();
    static Kart justKart = new Kart();
    static Hesap justHesap = new Hesap();
    static HesapHareketi justHesapHareketi = new HesapHareketi();

    public static ArrayList<HesapHareketi> justHesapHareketList = new ArrayList<>();

    public static void main(String[] args) {
        Gson gson = new GsonBuilder().create();
        Musteri musteri = new Musteri();
        AllObject allObject = new AllObject();

        createMusteri(musteri);
        createAllObject(musteri, allObject);

        String str = "";
        try {
            String adr = "http://localhost:8080/BankaServisi/inventory/musteri/kayit";
            URL url = new URL(adr);
            URLConnection con = url.openConnection();
            con.setDoOutput(true);
            con.setRequestProperty("Content-Type", "application/json");

            BufferedWriter outwrite = new BufferedWriter(new OutputStreamWriter(con.getOutputStream()));
            String allObjectStr = gson.toJson(allObject, AllObject.class);
            outwrite.append(allObjectStr);
            outwrite.flush();

            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            StringBuilder build = new StringBuilder();
            str = in.readLine();
            System.out.println("str : " + str);
            while ((str = in.readLine()) != null) {
                build.append(str).append("\r\n");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void createMusteri(Musteri musteri) {
        musteri.setMusteriAdi("Elif");
        musteri.setMusteriSoyadi("Temel");
        musteri.setMusteriTcNo("33311122244");
        musteri.setCinsiyet("Kiz");
        musteri.setSifre("12345");

        Kart kart = new Kart();
        kart.setKartNumarasi((long) (Math.random() * (100000)));
//        kart.setKartNumarasi((long) Math.random() * (100000));

        Hesap hesap = new Hesap();
        hesap.setBankaAdi("SE Banking");
        hesap.setHesapFullAd(musteri.getMusteriAdi() + " " + musteri.getMusteriSoyadi() + "");
        hesap.setHesapLimiti(1000);
        hesap.setHesapBakiye(400);
        hesap.setHesapTipi("Vadesiz");
        hesap.setHesapNo((long) (Math.random() * (100000)));

        HesapHareketi hesapHareketi = new HesapHareketi();
        hesapHareketi.setHareketTipi(HesapHareketi.PARA_YATIRMA);
        hesapHareketi.setHareketDegeri(200);
        hesap.setHesapBakiye(hesap.getHesapBakiye() + hesapHareketi.getHareketDegeri()); // +100 eklenir
        Date today = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy hh:mm:ss"); // tarih formatı - değiştirilebilir
        String bugun = sdf.format(today);
        hesapHareketi.setHareketTarihi(bugun);

        ArrayList<HesapHareketi> hesapHareketListesi = new ArrayList<>();
        hesapHareketListesi.add(hesapHareketi);
        hesap.setHesapHareketListesi(hesapHareketListesi); /// Hesap - HesapHareketi
        hesapHareketi.setHesap(hesap); /// Hesap - HesapHareketi

        ArrayList<Hesap> hesapList = new ArrayList<>();
        hesapList.add(hesap);
        kart.setHesapList(hesapList); /// Kart - Hesap
        hesap.setKart(kart); /// Kart - Hesap

        ArrayList<Kart> kartlar = new ArrayList<>();
        kartlar.add(kart);
        musteri.setKartList(kartlar); /// Musteri - Kart
        kart.setMusteri(musteri); /// Musteri - Kart
    }

    public static void createAllObject(Musteri realMusteri, AllObject all) {
        Kart kart = new Kart();
        Hesap hesap = new Hesap();
        ArrayList<Kart> tmpKartList = realMusteri.getKartList();
        ArrayList<Hesap> tmpHesapList = realMusteri.getKartList().get(0).getHesapList();
        ArrayList<HesapHareketi> tmpHHL = (ArrayList<HesapHareketi>) tmpHesapList.get(0).getHesapHareketListesi();

        Musteri musteri = new Musteri();
        musteri.setMusteriAdi(realMusteri.getMusteriAdi());
        musteri.setMusteriSoyadi(realMusteri.getMusteriSoyadi());
        musteri.setMusteriTcNo(realMusteri.getMusteriTcNo());
        musteri.setCinsiyet(realMusteri.getCinsiyet());
        musteri.setSifre(realMusteri.getSifre());
        all.setMusteri(musteri);

        kart.setKartNumarasi(tmpKartList.get(0).getKartNumarasi());
        all.setKart(kart);

        if (tmpHesapList != null && tmpHesapList.size() != 0) {
            Hesap tmpHesap = tmpHesapList.get(0);
            hesap.setBankaAdi(tmpHesap.getBankaAdi());
            hesap.setHesapFullAd(tmpHesap.getHesapFullAd());
            hesap.setHesapLimiti(tmpHesap.getHesapLimiti());
            hesap.setHesapBakiye(tmpHesap.getHesapBakiye());
            hesap.setHesapTipi(tmpHesap.getHesapTipi());
            hesap.setHesapNo(tmpHesap.getHesapNo());
            all.setHesap(hesap);
        }

        ArrayList<HesapHareketi> hesapHareketiList = new ArrayList<>();
        if (tmpHHL != null && tmpHHL.size() != 0) {
            for (HesapHareketi tmpHH : tmpHHL) {
                HesapHareketi hh = new HesapHareketi();
                hh.setHareketTipi(tmpHH.getHareketTipi());
                hh.setHareketDegeri(tmpHH.getHareketDegeri());
                hh.setHareketTarihi(tmpHH.getHareketTarihi());
                hesapHareketiList.add(hh);
            }
        }
        all.setHesapHareketListesi(hesapHareketiList);
        System.out.println("1");
    }
}
