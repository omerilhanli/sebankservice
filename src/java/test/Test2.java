package test;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import entity.Hesap;
import entity.HesapHareketi;
import entity.Kart;
import entity.Musteri;

public class Test2 {

    public static void main(String[] args) {

        Musteri musteri = new Musteri();
        createMusteri(musteri);

        String str = "";
        try {
            String adr = "http://localhost:8080/BankaServisi/inventory/musteri/kayit";
            URL url = new URL(adr);
            URLConnection con = url.openConnection();
            con.setDoOutput(true);
            con.setRequestProperty("Content-Type", "application/json");

            BufferedWriter outwrite = new BufferedWriter(new OutputStreamWriter(con.getOutputStream()));

            String kartArrStr = "", hesapArrStr = "", hesapHareketArrStr = "";
            for (int i = 0; i < musteri.getKartList().size(); i++) {
                Kart kart = musteri.getKartList().get(i);

                kartArrStr = kartArrStr.concat("{ \"kartNumarasi\" : " + kart.getKartNumarasi() + " },");

                for (int j = 0; j < kart.getHesapList().size(); j++) {
                    Hesap hesap = kart.getHesapList().get(j);

                    hesapArrStr = hesapArrStr.concat("{ \"hesapFullAd\" : \"" + hesap.getHesapFullAd() + "\" },");
                    hesapArrStr = hesapArrStr.concat("{ \"hesapTipi\" : \"" + hesap.getHesapTipi() + "\" },");
                    hesapArrStr = hesapArrStr.concat("{ \"hesapNo\" : \"" + hesap.getHesapNo() + "\" },");
                    hesapArrStr = hesapArrStr.concat("{ \"bankaAdi\" : \"" + hesap.getBankaAdi() + "\" },");
                    hesapArrStr = hesapArrStr.concat("{ \"hesapBakiye\" : \"" + hesap.getHesapBakiye() + "\" },");
                    hesapArrStr = hesapArrStr.concat("{ \"hesapLimiti\" : \"" + hesap.getHesapLimiti() + "\" },");

                    for (int k = 0; k < hesap.getHesapHareketListesi().size(); k++) {
                        HesapHareketi hesapHareketi = hesap.getHesapHareketListesi().get(k);
                        hesapHareketArrStr = hesapHareketArrStr.concat("{ \"hareketTipi\" : \"" + hesapHareketi.getHareketTipi() + "\" },");
                        hesapHareketArrStr = hesapHareketArrStr.concat("{ \"hareketDegeri\" : \"" + hesapHareketi.getHareketDegeri() + "\" },");
                        hesapHareketArrStr = hesapHareketArrStr.concat("{ \"hareketTarihi\" : \"" + hesapHareketi.getHareketTarihi() + "\" }");
                    }
                    hesapArrStr = hesapArrStr.concat("{ \"hesapHareketList\" : [ " + hesapHareketArrStr + " ] }");
                }

                kartArrStr = kartArrStr.concat("{ \"hesapList\" : [ " + hesapArrStr + " ] }");
            }

            String musteriStr = "{ "
                    + "\"musteriAdi\" : \"" + musteri.getMusteriAdi() + "\","
                    + "\"musteriSoyadi\" : \"" + musteri.getMusteriSoyadi() + "\","
                    + "\"musteriTcNo\" : \"" + musteri.getMusteriTcNo() + "\","
                    + "\"sifre\" : \"" + musteri.getSifre() + "\","
                    + "\"cinsiyet\" : \"" + musteri.getCinsiyet() + "\","
                    + " \"kartList\" : [ "
                    + kartArrStr
                    + " ] "
                    + " }";

            System.out.println("musteriStr : " + musteriStr);
            outwrite.append(musteriStr);

            outwrite.flush();
            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            StringBuilder build = new StringBuilder();
            while ((str = in.readLine()) != null) {
                build.append(str).append("\r\n");
            }
            System.out.println("Persist:" + str);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void createMusteri(Musteri musteri) {
        musteri.setMusteriAdi("Elif");
        musteri.setMusteriSoyadi("Temel");
        musteri.setMusteriTcNo("33311122244");
        musteri.setCinsiyet("Kız");
        musteri.setSifre("12345");

        Kart kart = new Kart();
        kart.setKartNumarasi((long) Math.random() * (100000));

        Hesap hesap = new Hesap();
        hesap.setBankaAdi("SE Banking");
        hesap.setHesapFullAd(musteri.getMusteriAdi() + " " + musteri.getMusteriSoyadi() + "");
        hesap.setHesapLimiti(1000);
        hesap.setHesapBakiye(400);
        hesap.setHesapTipi("Vadesiz");
        hesap.setHesapNo((long) Math.random() * (100000));

        HesapHareketi hesapHareketi = new HesapHareketi();
        hesapHareketi.setHareketTipi(HesapHareketi.PARA_YATIRMA);
        hesapHareketi.setHareketDegeri(200);
        hesap.setHesapBakiye(hesap.getHesapBakiye() + hesapHareketi.getHareketDegeri()); // +100 eklenir
        Date today = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy hh:mm:ss"); // tarih formatı - değiştirilebilir
        String bugun = sdf.format(today);
        hesapHareketi.setHareketTarihi(bugun);

        ArrayList<HesapHareketi> hesapHareketListesi = new ArrayList<>();
        hesapHareketListesi.add(hesapHareketi);
        hesap.setHesapHareketListesi(hesapHareketListesi); /// Hesap - HesapHareketi
        hesapHareketi.setHesap(hesap); /// Hesap - HesapHareketi

        ArrayList<Hesap> hesapList = new ArrayList<>();
        hesapList.add(hesap);
        kart.setHesapList(hesapList); /// Kart - Hesap
        hesap.setKart(kart); /// Kart - Hesap

        ArrayList<Kart> kartlar = new ArrayList<>();
        kartlar.add(kart);
        musteri.setKartList(kartlar); /// Musteri - Kart
        kart.setMusteri(musteri); /// Musteri - Kart
    }
}
