package test;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import entity.Hesap;
import entity.HesapHareketi;
import entity.Kart;
import entity.Musteri;
import repository.MusteriRepository;

public class Test3 {
  public static void main(String[] args) {

        Musteri musteri = new Musteri();        
        createMusteri(musteri);

        MusteriRepository repo = new MusteriRepository();
        repo.persist(musteri); // SUCCESS
        repo.close();
    }
    
    public static void createMusteri(Musteri musteri){
        musteri.setMusteriAdi("Omer-3");
        musteri.setMusteriSoyadi("İlhanlı-3");
        musteri.setMusteriTcNo("11122233344-3");
        musteri.setCinsiyet("Erkek-3");
        musteri.setSifre("1234-3");
    }
}
