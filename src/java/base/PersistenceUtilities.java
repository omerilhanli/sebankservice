package base;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class PersistenceUtilities {

    public static EntityManagerFactory factory;

    public static EntityManagerFactory getFactory() {
        // factory null ise bir adet instance üretilir. Bu kontrol sayesinde her class kendine göre bi factory üretemez.
        if (factory == null) {
            factory = Persistence.createEntityManagerFactory("BankaServisiPU");
        }
        return factory;
    }

}
