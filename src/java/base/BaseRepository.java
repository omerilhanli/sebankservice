package base;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.Query;

/*
    - DB'deki tüm CRUD işlemlerinin EntityManager ile yapıldığı controller class'ıdır.
    - Bu class'da Generic kullanılmıştır, sebebi tüm objelerin db'yle olan 
    ilişkilerini ayarlayan fonsiyon sınıfının türden bağımsız olması gereğidir. 
    Bu sayede code ilerde daha rahat bir şekilde güncellenebilir, yeni objeler eklenebilir 
    yada bazıları sistemden çıkarılabilir.
    
    - Bu class'ta EntityManager'ın  Persistence ayarları PersistenceUtilities class'ında yapılır ve
    bir adet instance üretilerek ilgili işlemlerin hepsinde ayrı ayrı kullanılabilir.
*/
public class BaseRepository<E extends BaseEntity> {

    // İlgili dataları db'den çekmek için oluşturulmuş standart sorgu stringi
    private String Select = "select %s from %s as %s";
    protected EntityManager entityManager;
    protected Class<E> entityClass;

    public BaseRepository(Class<E> entityClass) {
        this.entityClass = entityClass;
        entityManager = PersistenceUtilities.getFactory().createEntityManager();
    }

    public void close() { // Kaynak tüketimini engellemek için her açılan db bağlantısı kapatılmalıdır
        entityManager.close();
    }

    /*
        // kaydı sağlar, commite kadar tüm herşeye dbye atılır, 
        problem çıkarsa iptal olur, çıkmaz ve commit olursa kesin olarak atılır
    */
    public void persist(E entity) {
        entityManager.getTransaction().begin();
        entityManager.persist(entity);
        entityManager.getTransaction().commit();
    }

    
    public void merge(E entity) { // update sağlar
        entityManager.getTransaction().begin();
        entityManager.merge(entity);
        entityManager.getTransaction().commit();
    }

    public void remove(long entityId) {
        E entity = entityManager.getReference(entityClass, entityId);
        entityManager.getTransaction().begin();
        entityManager.remove(entity);
        entityManager.getTransaction().commit();
    }

    public E find(long entityId) { // id alıp obje döndürür dbden
        return entityManager.getReference(entityClass, entityId);
    }

    public List<E> list() { 
        String qL = createSelectSQL();
        Query query = entityManager.createQuery(qL);
        return query.getResultList();
    }

 // ORM gereği JPA kullandığımızdan dolayı sql dilimiz, JPQL olur.
    // JPQL biraz daha OOP prensibinde yazılır. Object bazlı yazılır.
    public String createSelectSQL() {
        String entityName = entityClass.getSimpleName();
        String variableName = entityName.toLowerCase(Locale.US);
        String jpql = String.format(Select, variableName, entityName, variableName);
        return jpql;
    }    
   
}
