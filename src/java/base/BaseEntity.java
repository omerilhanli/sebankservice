package base;

import java.io.Serializable;
import javax.persistence.MappedSuperclass;

/*  
    Her Entity olacak object'in, DB'ye ORM aracı tarafından gönderilebilmesi için
    @MappedSuperclass annotation'ı ile işaretlenmesi zorunludur. Mappingi sağlar

    // Tüm objeler BaseEntity objesinden türer
*/
@MappedSuperclass 
public class BaseEntity implements Serializable {

}
