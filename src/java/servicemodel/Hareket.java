package servicemodel;

import java.io.Serializable;

/**
 * // Gson tarafından Json Stringinin objeye map edilmesi sırasında,
 * composition gereği parent-child ilişkileri kurduğumuzdan Cycle'a düşüp, StackOverFlow Exception'ınına düşmemek için
 * bu object kullanılır
 * 
 * @author omerilhanli
 */
public class Hareket implements Serializable {

    private String sahipHesapNo;
    private String aliciHesapNo;
    private String aliciTCNo;
    private String miktar;
    private String hareketTipi;
    private String hareketKilitDurumu;

    public String getSahipHesapNo() {
        return sahipHesapNo;
    }

    public void setSahipHesapNo(String sahipHesapNo) {
        this.sahipHesapNo = sahipHesapNo;
    }

    public String getAliciHesapNo() {
        return aliciHesapNo;
    }

    public void setAliciHesapNo(String aliciHesapNo) {
        this.aliciHesapNo = aliciHesapNo;
    }

    public String getAliciTCNo() {
        return aliciTCNo;
    }

    public void setAliciTCNo(String aliciTCNo) {
        this.aliciTCNo = aliciTCNo;
    }

    public String getMiktar() {
        return miktar;
    }

    public void setMiktar(String miktar) {
        this.miktar = miktar;
    }

    public String getHareketTipi() {
        return hareketTipi;
    }

    public void setHareketTipi(String hareketTipi) {
        this.hareketTipi = hareketTipi;
    }

    public void setHareketKilitDurumu(String hareketKilitDurumu) {
        this.hareketKilitDurumu = hareketKilitDurumu;
    }

    public String getHareketKilitDurumu() {
        return hareketKilitDurumu;
    }

}
