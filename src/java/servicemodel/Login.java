package servicemodel;

import java.io.Serializable;

/**
 * * // Gson tarafından Json Stringinin objeye map edilmesi sırasında,
 * composition gereği parent-child ilişkileri kurduğumuzdan Cycle'a düşüp,
 * StackOverFlow Exception'ınına düşmemek için bu object kullanılır
 *
 * @author omerilhanli
 */
public class Login implements Serializable {

    private String tcNo;
    private String sifre;

    public String getTcNo() {
        return tcNo;
    }

    public void setTcNo(String tcNo) {
        this.tcNo = tcNo;
    }

    public String getSifre() {
        return sifre;
    }

    public void setSifre(String sifre) {
        this.sifre = sifre;
    }

}
