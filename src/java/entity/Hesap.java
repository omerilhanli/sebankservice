package entity;

import base.BaseEntity;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "Hesap")
public class Hesap extends BaseEntity {

    @Id // ORM gereği, Entity olacak objenin bir ID'si olmak zorundadır.
    @GeneratedValue(strategy = GenerationType.IDENTITY) // ID'ye Primary key sağlar.
    @Column(name = "hesapId") // DB'de tablo kolon ismi olarak ayarlamayı sağlar.
    private long hesapId;
    @Column(name = "hesapFullAd")
    private String hesapFullAd;
    @Column(name = "hesapTipi") // bu annotation eklenmezse, hesapTipi, DB'de HESAP_TİPİ olarak eklenmeye çalışılır 
    // Öyle olunca da türkçe karakter yüzünden hata alınır.
    private String hesapTipi;
    @Column(name = "hesapNo", unique = true) // Her hesap için hesap numarası unique yani tekil olmalıdır.
    private long hesapNo;
    @Column(name = "hesapFaizOrani")
    private double hesapFaizOrani;
    @Column(name = "sonEklenenFaizTarihi")
    private String sonEklenenFaizTarihi="-";
    @Column(name = "bankaAdi")
    private String bankaAdi;
    @Column(name = "hesapBakiye")
    private int hesapBakiye;
    @Column(name = "hesapLimiti")
    private int hesapLimiti;

    @ManyToOne // Her child objede, @OneToMany'ye karşılık olarak @ManyToOne ile parent obje referansı yazılır
    @JoinColumn(name = "kartId")
    private Kart kart;

    // Her Parent objenin childları olur, o yüzden child'lardan oluşan bir liste @OneToMany olarak eklenmelidir
    @OneToMany(mappedBy = "hesap", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<HesapHareketi> hesapHareketListesi;

    public Hesap() {
    }

    //<editor-fold defaultstate="collapsed" desc="GETTER & SETTER">
    public long getHesapId() {
        return hesapId;
    }

    public void setHesapId(long hesapId) {
        this.hesapId = hesapId;
    }

    public String getHesapFullAd() {
        return hesapFullAd;
    }

    public void setHesapFullAd(String hesapFullAd) {
        this.hesapFullAd = hesapFullAd;
    }

    public String getHesapTipi() {
        return hesapTipi;
    }

    public void setHesapTipi(String hesapTipi) {
        this.hesapTipi = hesapTipi;
    }

    public long getHesapNo() {
        return hesapNo;
    }

    public void setHesapNo(long hesapNo) {
        this.hesapNo = hesapNo;
    }

    public double getHesapFaizOrani() {
        return hesapFaizOrani;
    }

    public void setHesapFaizOrani(double hesapFaizOrani) {
        this.hesapFaizOrani = hesapFaizOrani;
    }

    public String getBankaAdi() {
        return bankaAdi;
    }

    public void setBankaAdi(String bankaAdi) {
        this.bankaAdi = bankaAdi;
    }

    public int getHesapBakiye() {
        return hesapBakiye;
    }

    public void setHesapBakiye(int hesapBakiye) {
        this.hesapBakiye = hesapBakiye;
    }

    public int getHesapLimiti() {
        return hesapLimiti;
    }

    public void setHesapLimiti(int hesapLimiti) {
        this.hesapLimiti = hesapLimiti;
    }

    public Kart getKart() {
        return kart;
    }

    public void setKart(Kart kart) {
        this.kart = kart;
    }

    public List<HesapHareketi> getHesapHareketListesi() {
        return hesapHareketListesi;
    }

    public void setHesapHareketListesi(List<HesapHareketi> hesapHareketListesi) {
        this.hesapHareketListesi = hesapHareketListesi;
    }

    public void setSonEklenenFaizTarihi(String sonEklenenFaizTarihi) {
        this.sonEklenenFaizTarihi = sonEklenenFaizTarihi;
    }

    public String getSonEklenenFaizTarihi() {
        return sonEklenenFaizTarihi;
    }

    //</editor-fold>
}
