package entity;

import java.io.Serializable;
import java.util.ArrayList;

/*
    Tüm objeler Serializable ile implement edilir, ORM aracı object akımlarında Serializable iplmeneti mecbur tutar
*/
public class AllObject implements Serializable {
    
    //<editor-fold defaultstate="collapsed" desc="Data">
    private  Musteri musteri = new Musteri();
    private  Kart kart = new Kart();
    private  Hesap hesap = new Hesap();
    private  ArrayList<HesapHareketi> hesapHareketListesi = new ArrayList<>();
    private  ArrayList<Hesap> hesapListesi = new ArrayList<>();
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Getter - Setter">
    public Musteri getMusteri() {
        return musteri;
    }
    
    public void setMusteri(Musteri musteri) {
        this.musteri = musteri;
    }
    
    public Kart getKart() {
        return kart;
    }
    
    public void setKart(Kart kart) {
        this.kart = kart;
    }
    
    public Hesap getHesap() {
        return hesap;
    }
    
    public void setHesap(Hesap hesap) {
        this.hesap = hesap;
    }
    
    public ArrayList<HesapHareketi> getHesapHareketListesi() {
        return hesapHareketListesi;
    }
    
    public void setHesapHareketListesi(ArrayList<HesapHareketi> hesapHareketListesi) {
        this.hesapHareketListesi = hesapHareketListesi;
    }
    
    public ArrayList<Hesap> getHesapListesi() {
        return hesapListesi;
    }
    
    public void setHesapListesi(ArrayList<Hesap> hesapListesi) {
        this.hesapListesi = hesapListesi;
    }
//</editor-fold>
   
}
