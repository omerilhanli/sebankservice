package entity;

import base.BaseEntity;
import java.util.ArrayList;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity @Table(name="Kart")
public class Kart extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "kartId")
    private long kartId;
    @Column(name = "kartNumarasi", unique = true)
    private long kartNumarasi;
   

    @ManyToOne
    @JoinColumn(name = "musteriId")
    private Musteri musteri;
    
    @OneToMany(mappedBy = "kart", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private ArrayList<Hesap> hesapList;

    public Kart() {

    }

    public Kart(long kartNumarasi) {
        this.kartNumarasi = kartNumarasi;
    }

    public Kart(long kartNumarasi, Musteri musteri) {
        this.kartNumarasi = kartNumarasi;
        this.musteri = musteri;
    }

    public long getKartId() {
        return kartId;
    }

    public void setKartId(long kartId) {
        this.kartId = kartId;
    }

    public long getKartNumarasi() {
        return kartNumarasi;
    }

    public void setKartNumarasi(long kartNumarasi) {
        this.kartNumarasi = kartNumarasi;
    }

    public Musteri getMusteri() {
        return musteri;
    }

    public void setMusteri(Musteri musteri) {
        this.musteri = musteri;
    }

    public ArrayList<Hesap> getHesapList() {
        return hesapList;
    }

    public void setHesapList(ArrayList<Hesap> hesapList) {
        this.hesapList = hesapList;
    }

}
