package entity;

import base.BaseEntity;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "HesapHareketi")
public class HesapHareketi extends BaseEntity {

    public static final String PARA_CEKME = "Para_Cekme",
            PARA_YATIRMA = "Para_Yatırma",
            PARA_TRANSFERI = "Para_Transferi",
            FAIZ_EKLE="Faiz_Ekle",
            HESAP_KILIT = "Hesap_Koruma_Altinda",
            HESAP_SERBEST = "Hesap_Serbest";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "hareketId")
    private long hareketId;
    @Column(name = "hareketTipi")
    private String hareketTipi;
    @Column(name = "hareketDegeri")
    private int hareketDegeri;
    @Column(name = "hareketTarihi")
    private String hareketTarihi;
    @Column(name = "hesapKilitDurumu")
    private String hesapKilitDurumu;

    @ManyToOne
    @JoinColumn(name = "hesapId")
    private Hesap hesap;

    public HesapHareketi() {
    }

    //<editor-fold defaultstate="collapsed" desc="Getter & Setter">
    public long getHareketId() {
        return hareketId;
    }

    public void setHareketId(long hareketId) {
        this.hareketId = hareketId;
    }

    public String getHareketTipi() {
        return hareketTipi;
    }

    public void setHareketTipi(String hareketTipi) {
        this.hareketTipi = hareketTipi;
    }

    public int getHareketDegeri() {
        return hareketDegeri;
    }

    public void setHareketDegeri(int hareketDegeri) {
        this.hareketDegeri = hareketDegeri;
    }

    public String getHareketTarihi() {
        return hareketTarihi;
    }

    public void setHareketTarihi(String hareketTarihi) {
        this.hareketTarihi = hareketTarihi;
    }

    public Hesap getHesap() {
        return hesap;
    }

    public void setHesap(Hesap hesap) {
        this.hesap = hesap;
    }

    public String getHesapKilitDurumu() {
        return hesapKilitDurumu;
    }

    public void setHesapKilitDurumu(String hesapKilitDurumu) {
        this.hesapKilitDurumu = hesapKilitDurumu;
    }
//</editor-fold>
}
