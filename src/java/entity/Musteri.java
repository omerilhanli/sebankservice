package entity;

import base.BaseEntity;
import java.util.ArrayList;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "Musteri")
public class Musteri extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "musteriId")
    private long musteriId;
    @Column(name = "musteriAdi")
    private String musteriAdi;
    @Column(name = "musteriSoyadi")
    private String musteriSoyadi;
    @Column(name = "musteriTcNo")
    private String musteriTcNo;
    @Column(name = "cinsiyet")
    private String cinsiyet;
    @Column(name = "sifre")
    private String sifre;
    @Column(name = "musteriKayitTarihi")
    private String musteriKayitTarihi;

    @OneToMany(mappedBy = "musteri", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private ArrayList<Kart> kartList;

    public Musteri() {

    }

    public long getMusteriId() {
        return musteriId;
    }

    public void setMusteriId(long musteriId) {
        this.musteriId = musteriId;
    }

    public String getMusteriAdi() {
        return musteriAdi;
    }

    public void setMusteriAdi(String musteriAdi) {
        this.musteriAdi = musteriAdi;
    }

    public String getMusteriSoyadi() {
        return musteriSoyadi;
    }

    public void setMusteriSoyadi(String musteriSoyadi) {
        this.musteriSoyadi = musteriSoyadi;
    }

    public String getMusteriTcNo() {
        return musteriTcNo;
    }

    public void setMusteriTcNo(String musteriTcNo) {
        this.musteriTcNo = musteriTcNo;
    }

    public String getCinsiyet() {
        return cinsiyet;
    }

    public void setCinsiyet(String cinsiyet) {
        this.cinsiyet = cinsiyet;
    }

    public String getSifre() {
        return sifre;
    }

    public void setSifre(String sifre) {
        this.sifre = sifre;
    }

    public ArrayList<Kart> getKartList() {
        return kartList;
    }

    public void setKartList(ArrayList<Kart> kartList) {
        this.kartList = kartList;
    }

    public String getMusteriKayitTarihi() {
        return musteriKayitTarihi;
    }

    public void setMusteriKayitTarihi(String musteriKayitTarihi) {
        this.musteriKayitTarihi = musteriKayitTarihi;
    }

}
