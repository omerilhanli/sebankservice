package service;

import java.util.HashSet;
import java.util.Set;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/*
    - Her servis class'ı için ApplicationPath ayarlanır, bu path ilgili yerlerde request çağrılarında kullanılır
*/
@ApplicationPath("inventory")
public class InventoryApplication extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new HashSet<>();
        resources.add(MusteriResource.class); // Her servis class'ı resources listesine eklenmelidir.
        return resources;
    }

}
