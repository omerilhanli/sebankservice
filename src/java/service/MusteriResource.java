package service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.json.Json;
import javax.json.JsonObject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import entity.AllObject;
import entity.Hesap;
import entity.HesapHareketi;
import entity.Kart;
import servicemodel.Login;
import entity.Musteri;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import org.jboss.logging.Param;
import repository.MusteriRepository;
import servicemodel.Hareket;

@Path("musteri") // Her Servis class'ını birbirinden ayıran path belirlenmelidir.
public class MusteriResource {

    /**
     * String şeklinde AllObject Stringi alır, onu Musteri objesine convert eder, DB'ye kayıt ederiz.
     * @param allObjectStr alır
     * @return String "Müşteri Kaydedildi."
     */
    @POST // Request tipimiz
    @Consumes(MediaType.APPLICATION_JSON) // Tüm POST requestlerde, clienttan gelen datayı tüketimi(response) sağlar
    @Produces(MediaType.APPLICATION_JSON) // Tüm GET requestlerde, client tarafına data üretimi sağlar
    @Path("kayit") // servis methodlarını birbirinden ayıran path
    public String saveToDB(String allObjectStr) {
        System.out.println("allObjectStr : " + allObjectStr);
        Gson gson = new GsonBuilder().create();
        AllObject allObject = gson.fromJson(allObjectStr, AllObject.class);
        Musteri tmpMusteri = allObject.getMusteri();

        Musteri musteri = new Musteri();
        Kart kart = new Kart();
        Hesap hesap = new Hesap();

        ArrayList<Kart> kartList = new ArrayList<>();
        ArrayList<Hesap> hesapList = new ArrayList<>();
        ArrayList<HesapHareketi> hesapHareketList = new ArrayList<>();

        musteri.setMusteriAdi(tmpMusteri.getMusteriAdi());
        musteri.setMusteriSoyadi(tmpMusteri.getMusteriSoyadi());
        musteri.setMusteriTcNo(tmpMusteri.getMusteriTcNo());
        musteri.setCinsiyet(tmpMusteri.getCinsiyet());
        musteri.setSifre(tmpMusteri.getSifre());
        musteri.setMusteriKayitTarihi(today());

        Kart tmpKart = allObject.getKart();
        kart.setKartNumarasi(tmpKart.getKartNumarasi());

        Hesap tmpHesap = allObject.getHesap();
        hesap.setBankaAdi(tmpHesap.getBankaAdi());
        hesap.setHesapFullAd(tmpHesap.getHesapFullAd());
        hesap.setHesapLimiti(tmpHesap.getHesapLimiti());
        hesap.setHesapBakiye(tmpHesap.getHesapBakiye());
        hesap.setHesapTipi(tmpHesap.getHesapTipi());
        hesap.setSonEklenenFaizTarihi(tmpHesap.getSonEklenenFaizTarihi());
        hesap.setHesapFaizOrani(tmpHesap.getHesapFaizOrani());
        hesap.setHesapNo(tmpHesap.getHesapNo());
        hesap.setKart(kart);/// Kart - Hesap
        hesapList.add(hesap);
        kart.setHesapList(hesapList); /// Kart - Hesap

        for (HesapHareketi hh : allObject.getHesapHareketListesi()) {
            hh.setHesap(hesap); /// Hesap - HesapHareketi
            hh.setHesapKilitDurumu(HesapHareketi.HESAP_SERBEST);
            hesapHareketList.add(hh);
        }
        hesap.setHesapHareketListesi(hesapHareketList); /// Hesap - HesapHareketi

        kartList.add(kart);
        kart.setMusteri(musteri); /// Musteri - Kart
        musteri.setKartList(kartList); /// Musteri - Kart

        MusteriRepository repo = new MusteriRepository();        
        repo.persist(musteri); // müşteri dbde persist edildiği anda, müşterinin id'si otomatik güncellenir       
        repo.close(); // en son ki kayıttan sonraki kayıt olarak, 
        // yani mesela 3 kayıt vardı, bu kaydı attık 4 oldu, is=4 olarak güncellenir

        return "Müşteri Kaydedildi.";
    }

    /**
     * Login Stringi alır onu Gson ile Login objesine convert eder db'de ilgili 
     * kontrolle TRUE yada FALSE String responsu döneriz.
     * @param loginStr
     * @return 
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("giris")
    public String getLogin(String loginStr) {
        System.out.println("loginStr : " + loginStr);
        String FALSE = "false";
        Gson gson = new GsonBuilder().create();
        String allObjectStr = FALSE;

        Login login = gson.fromJson(loginStr, Login.class);
        String tcNo = login.getTcNo();
        String sifre = login.getSifre();

        System.out.println("tcNo : " + tcNo);
        System.out.println("sifre : " + sifre);

        MusteriRepository repo = new MusteriRepository();
        List<Musteri> musteriList = repo.list();
        repo.close();
        if (tcNo.isEmpty() || sifre.isEmpty()) {
            return allObjectStr;
        }

        for (Musteri musteri : musteriList) {
            System.out.println("musteri.getMusteriTcNo : " + musteri.getMusteriTcNo());
            System.out.println("musteri.getSifre : " + musteri.getSifre());
            if (musteri.getMusteriTcNo().equals(tcNo)
                    && musteri.getSifre().equals(sifre)) {

                AllObject all = new AllObject();
                createAllObject(musteri, all);

                allObjectStr = gson.toJson(all, AllObject.class);
                return allObjectStr;

            } else if (!musteri.getMusteriTcNo().equals(tcNo)
                    || !musteri.getSifre().equals(sifre)) {
                allObjectStr = FALSE;
            }
        }

        return allObjectStr;
    }
    
    /**
     * Hareket Stringi alır, Gson ile Hareket objesine çeviririz. Sonra ilgili istek tipine göre
     * (Para Çekme, Para Yatırma, Faiz Ekleme) ilgili update işlemini gerçekleştiririz.
     * @param hareketStr
     * @return 
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("paraTransferHesabimda")
    public String getParaTransferHesabimda(String hareketStr) {
        System.out.println("hareketStr : " + hareketStr);
        final String TRUE = "true", FALSE = "false";
        String response = FALSE;
        MusteriRepository repo = new MusteriRepository();

        Hareket hareket = createHareket(hareketStr);
        List<Musteri> musteriList = repo.list();
        for (Musteri musteri : musteriList) {
            for (Kart kart : musteri.getKartList()) {
                for (Hesap hesap : kart.getHesapList()) {
                    if (hareket.getSahipHesapNo().equals("" + hesap.getHesapNo())) {
                        response = updateMusteri(musteri, hesap, hareket, repo, hareket.getHareketTipi());
                        return response;
                    }
                }
            }
        }

        return response;
    }

    /**
     * Hareket Stringi alır, Gson ile Hareket objesine çeviririz. Obje içinde Alıcı ve Gönderici Hesap Nolarla
     * Para transferi gerçekleşir ve ilgili update yapılır.
     * @param hareketStr
     * @return 
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("paraTransferBaskaHesaba")
    public String getParaTransferBaskaHesaba(String hareketStr) {
        System.out.println("getParaTransfer : " + hareketStr);
        final String TRUE = "true", FALSE = "false";
        String status1 = FALSE, status2 = FALSE;
        String response = FALSE;

        MusteriRepository repo = new MusteriRepository();
        Gson gson = new GsonBuilder().create();
        Musteri musteri1 = new Musteri();
        Musteri musteri2 = new Musteri();
        Hareket hareket = createHareket(hareketStr);

        List<Musteri> musteriList = repo.list();
        for (Musteri musteri : musteriList) {
            for (Kart kart : musteri.getKartList()) {
                for (Hesap hesap : kart.getHesapList()) {
                    if (hareket.getSahipHesapNo().equals("" + hesap.getHesapNo())) { // Buraya giren hesap Sahip Musterinin
                        musteri1 = musteri;
                        hesap.setHesapBakiye(hesap.getHesapBakiye() - Integer.parseInt(hareket.getMiktar()));
                        HesapHareketi hesapHareketi = new HesapHareketi();
                        hesapHareketi.setHareketTipi(HesapHareketi.PARA_TRANSFERI + " yapılmıştır.");
                        hesapHareketi.setHesapKilitDurumu(hareket.getHareketKilitDurumu());
                        hesapHareketi.setHareketDegeri(Integer.parseInt(hareket.getMiktar()));
                        hesapHareketi.setHareketTarihi(today());
                        hesapHareketi.setHesap(hesap);
                        hesap.getHesapHareketListesi().add(hesapHareketi);

                        status1 = TRUE;
                    }

                    if (hareket.getAliciHesapNo().equals("" + hesap.getHesapNo())) { // Buraya giren hesap Alıcı Müşterinin
                        musteri2 = musteri;
                        hesap.setHesapBakiye(hesap.getHesapBakiye() + Integer.parseInt(hareket.getMiktar()));
                        HesapHareketi hesapHareketi = new HesapHareketi();
                        hesapHareketi.setHareketTipi(HesapHareketi.PARA_TRANSFERI + " alınmıştır.");
                        hesapHareketi.setHesapKilitDurumu(hareket.getHareketKilitDurumu());
                        hesapHareketi.setHareketDegeri(Integer.parseInt(hareket.getMiktar()));
                        hesapHareketi.setHareketTarihi(today());
                        hesapHareketi.setHesap(hesap);
                        hesap.getHesapHareketListesi().add(hesapHareketi);

                        status2 = TRUE;
                    }
                }
            }
        }

        System.out.println("status1:" + status1);
        System.out.println("status2:" + status2);
        if (status1.equals(TRUE) && status2.equals(TRUE)) { // Eğer ikisindende TRUE gelirse, transfer yapılır
            repo.merge(musteri1);
            repo.merge(musteri2);

            AllObject all = new AllObject();
            createAllObject(musteri1, all);
            response = gson.toJson(all, AllObject.class);
            return response; // response olarak AllObject obje stringi döndürülür

        } else if (status1.equals(FALSE) || status2.equals(FALSE)) { // Aksi halde
            response = FALSE; // Transfer gerçekleşmez ve FALSE stringi döndürülür.
        }
        repo.close();
        return response;
    }

    /**
     * Hareket Stringi Gson ile Hareket Objesine convert(mapping) edilir. İlgili işleme göre Musteri yeniden update edilir.
     * @param hareketStr
     * @return 
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("hesapKilitle")
    public String getHesapKilitle(String hareketStr) {
        System.out.println("uuugetHesapKilitle : " + hareketStr);
        final String TRUE = "true", FALSE = "false";

        MusteriRepository repo = new MusteriRepository();
        Hareket hareket = createHareket(hareketStr);
        System.out.println("");
        List<Musteri> musteriList = repo.list();
        for (Musteri musteri : musteriList) {
            for (Kart kart : musteri.getKartList()) {
                for (Hesap hesap : kart.getHesapList()) {
                    if (hareket.getSahipHesapNo().equals("" + hesap.getHesapNo())) {
                        
                        HesapHareketi hesapHareketi = new HesapHareketi();
                        System.out.println("asdasd : hesapKilit.getHareketTipi : " + hareket.getHareketTipi());
                        hesapHareketi.setHareketTipi(hareket.getHareketTipi());
                        hesapHareketi.setHesapKilitDurumu(hesapHareketi.getHesapKilitDurumu());
                        hesapHareketi.setHareketDegeri(Integer.parseInt(hareket.getMiktar()));
                        hesapHareketi.setHareketTarihi(today());
                        hesapHareketi.setHesap(hesap);
                        hesap.getHesapHareketListesi().add(hesapHareketi);

                        repo.merge(musteri);
                        return TRUE;
                    }
                }
            }
        }
        repo.close(); // Her işlemden sonra repo close edilmelidir. Kaynakları serbest bırakmak için.
        return FALSE;
    }

    /**
     * HesapNoId Stringi alınıp, ilgili hesap DB'den çekilir.
     * Eğer gelen hesapNoId ile bi hesap bulunmazsa yada hesapNoId sahip hesabına denk gelirse,
     * response olarak FALSE dönülür. Aksi halde response olarak ilgili hesabın numarası string olarak dönülür.
     * @param hesapNoId
     * @return 
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("hesapNoBul")
    public String getFindHesapNo(String hesapNoId) {
        System.out.println("hesapNoId : " + hesapNoId);
        String FALSE = "false", response = FALSE;
        int hesapNo = 0;
        int id = 0;
        if (hesapNoId.isEmpty()) {
            return response;
        } else {
            String idStr = hesapNoId.split(":")[0];
            hesapNo = Integer.parseInt(hesapNoId.split(":")[1]);
            id = Integer.parseInt(idStr);
        }

        MusteriRepository repo = new MusteriRepository();
        List<Musteri> musteriList = repo.list();
        for (Musteri musteri : musteriList) {
            for (Kart kart : musteri.getKartList()) {
                for (Hesap hesap : kart.getHesapList()) {
                    if (hesap.getHesapId() == id && hesap.getHesapNo() != hesapNo) {
                        return hesap.getHesapNo() + "";
                    }
                }
            }
        }
        return response;
    }

    /**
     * Hareket Stringi alır Hareket objesine map ederiz.
     * @param hareketStr
     * @return 
     */
    public Hareket createHareket(String hareketStr) {
        Hareket hareket = new Hareket();
        Gson gson = new GsonBuilder().create();
        Hareket tmpHareket = gson.fromJson(hareketStr, Hareket.class);
        String sahipHesapNo = tmpHareket.getSahipHesapNo();
        String aliciHesapNo = tmpHareket.getAliciHesapNo();
        String aliciTCNo = tmpHareket.getAliciTCNo();
        String miktar = tmpHareket.getMiktar();
        String hesapKilit = tmpHareket.getHareketTipi();
        System.out.println("asdasd : hesapKilit : " + hesapKilit);

        hareket.setSahipHesapNo(sahipHesapNo);
        hareket.setAliciHesapNo(aliciHesapNo);
        hareket.setAliciTCNo(aliciTCNo);
        hareket.setMiktar(miktar);
        hareket.setHareketTipi(hesapKilit);
        return hareket;
    }

    /**
     * İlgili parametrelerle Muşteri objesi update edilir.
     * @param musteri
     * @param hesap
     * @param hareket
     * @param repo
     * @param hareketTipi
     * @return 
     */
    public String updateMusteri(Musteri musteri, Hesap hesap, Hareket hareket, MusteriRepository repo, String hareketTipi) {
        System.out.println("updateMusteri-hareketTipi : " + hareketTipi);
        final String TRUE = "true", FALSE = "false";
        String allObjectStr = FALSE;
        Gson gson = new GsonBuilder().create();
        HesapHareketi hesapHareketi = new HesapHareketi();
        if (hareketTipi.equals(HesapHareketi.PARA_CEKME)) {
            hesapHareketi.setHareketDegeri(Integer.parseInt(hareket.getMiktar()));
            hesap.setHesapBakiye(hesap.getHesapBakiye() - Integer.parseInt(hareket.getMiktar()));
        } else if (hareketTipi.equals(HesapHareketi.PARA_YATIRMA)) {
            hesapHareketi.setHareketDegeri(Integer.parseInt(hareket.getMiktar()));
            hesap.setHesapBakiye(hesap.getHesapBakiye() + Integer.parseInt(hareket.getMiktar()));
        } else if (hareketTipi.equals(HesapHareketi.FAIZ_EKLE)) {
            hesapHareketi.setHareketDegeri(Integer.parseInt(hareket.getMiktar()));
            hesap.setHesapBakiye(hesap.getHesapBakiye() + Integer.parseInt(hareket.getMiktar()));
            hesap.setSonEklenenFaizTarihi(today());
        }

        hesapHareketi.setHareketTipi(hareketTipi);
        hesapHareketi.setHesapKilitDurumu(hareket.getHareketKilitDurumu());
        hesapHareketi.setHareketTarihi(today());
        hesapHareketi.setHesap(hesap);
        hesap.getHesapHareketListesi().add(hesapHareketi);

        repo.merge(musteri);
        repo.close();

        AllObject all = new AllObject();
        createAllObject(musteri, all);
        allObjectStr = gson.toJson(all, AllObject.class);
        return allObjectStr;
    }

    /**
     * Muşteri Objesi alınır, AllObject objesi içine map edilir.
     * @param realMusteri
     * @param all 
     */
    public void createAllObject(Musteri realMusteri, AllObject all) {
        Kart kart = new Kart();
        Hesap hesap = new Hesap();
        List<Kart> tmpKartList = realMusteri.getKartList();
        List<Hesap> tmpHesapList = realMusteri.getKartList().get(0).getHesapList();
        List<HesapHareketi> tmpHHL = tmpHesapList.get(0).getHesapHareketListesi();

        Musteri musteri = new Musteri();
        musteri.setMusteriAdi(realMusteri.getMusteriAdi());
        musteri.setMusteriSoyadi(realMusteri.getMusteriSoyadi());
        musteri.setMusteriTcNo(realMusteri.getMusteriTcNo());
        musteri.setCinsiyet(realMusteri.getCinsiyet());
        musteri.setSifre(realMusteri.getSifre());
        musteri.setMusteriKayitTarihi(realMusteri.getMusteriKayitTarihi());
        all.setMusteri(musteri);

        kart.setKartNumarasi(tmpKartList.get(0).getKartNumarasi());
        all.setKart(kart);

        // bu null check işlemidir. null gelmesi iht karşın yazılır.
        if (tmpHesapList != null && !tmpHesapList.isEmpty()) {
            Hesap tmpHesap = tmpHesapList.get(0);
            hesap.setBankaAdi(tmpHesap.getBankaAdi());
            hesap.setHesapFullAd(tmpHesap.getHesapFullAd());
            hesap.setHesapLimiti(tmpHesap.getHesapLimiti());
            hesap.setSonEklenenFaizTarihi(tmpHesap.getSonEklenenFaizTarihi());
            hesap.setHesapFaizOrani(tmpHesap.getHesapFaizOrani());
            hesap.setHesapBakiye(tmpHesap.getHesapBakiye());
            hesap.setHesapTipi(tmpHesap.getHesapTipi());
            hesap.setHesapNo(tmpHesap.getHesapNo());
            all.setHesap(hesap);
        }
        
        ArrayList<HesapHareketi> hesapHareketiList = new ArrayList<>();
        if (tmpHHL != null && tmpHHL.size() != 0) {
            for (HesapHareketi tmpHH : tmpHHL) {
                HesapHareketi hh = new HesapHareketi();
                hh.setHareketTipi(tmpHH.getHareketTipi());
                hh.setHesapKilitDurumu(tmpHH.getHesapKilitDurumu());
                hh.setHareketDegeri(tmpHH.getHareketDegeri());
                hh.setHareketTarihi(tmpHH.getHareketTarihi());
                hesapHareketiList.add(hh);
            }
        }
        all.setHesapHareketListesi(hesapHareketiList);
    }

    // Bugünün tarihi örnek : 10 Haziran 2017 11:22:54 şeklinde alınır.
    public String today() {
        return (new SimpleDateFormat("dd MMMM yyyy hh:mm:ss")).format(new Date());
    }

}
