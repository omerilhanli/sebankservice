package repository;

import base.BaseRepository;
import java.util.ArrayList;
import java.util.List;
import entity.Hesap;
import entity.Kart;
import entity.Musteri;

public class MusteriRepository extends BaseRepository<Musteri> {

    public MusteriRepository() {
        super(Musteri.class);
    }

    /**
     * TC Kimlik No ile Musteri objesi dbden alınır. Tüm liste çekiliyo içinden tc no ile eşleşen müşteri
     * alınır ve return edilir.
     * @param tcNo
     * @return 
     */
    public Musteri findUserByTCNO(String tcNo) {
        List<Musteri> musteriList = super.list();
        Musteri expectedUser = null;
        for (Musteri user : musteriList) {
            if (user.getMusteriTcNo().equals(tcNo)) {
                expectedUser = user;
                break;
            }
        }
        return expectedUser;
    }

    /**
     * Hesap listesi db'den çekiliyot
     * @return 
     */
    public ArrayList<Hesap> getHesapList() {
        ArrayList<Hesap> hesapList = new ArrayList<>();
        List<Musteri> musteriList = super.list(); // burda çekiliyor.
        for (Musteri musteri : musteriList) {
            for (Kart kart : musteri.getKartList()) {
                for (Hesap tmpHesap : kart.getHesapList()) {
                    Hesap hesap = new Hesap(); // cycle kurtarmak için yeni hesap oluşturup listeye ekleriz
                    hesap.setBankaAdi(tmpHesap.getBankaAdi());
                    hesap.setHesapFullAd(tmpHesap.getHesapFullAd());
                    hesap.setHesapLimiti(tmpHesap.getHesapLimiti());
                    hesap.setHesapBakiye(tmpHesap.getHesapBakiye());
                    hesap.setHesapTipi(tmpHesap.getHesapTipi());
                    hesap.setHesapNo(tmpHesap.getHesapNo());

                    hesapList.add(hesap);
                }
            }
        }
        return hesapList;
    }

}
