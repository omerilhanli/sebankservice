package entity;

import entity.Hesap;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-06-15T05:03:06")
@StaticMetamodel(HesapHareketi.class)
public class HesapHareketi_ { 

    public static volatile SingularAttribute<HesapHareketi, String> hareketTarihi;
    public static volatile SingularAttribute<HesapHareketi, String> hareketTipi;
    public static volatile SingularAttribute<HesapHareketi, Long> hareketId;
    public static volatile SingularAttribute<HesapHareketi, Integer> hareketDegeri;
    public static volatile SingularAttribute<HesapHareketi, Hesap> hesap;
    public static volatile SingularAttribute<HesapHareketi, String> hesapKilitDurumu;

}