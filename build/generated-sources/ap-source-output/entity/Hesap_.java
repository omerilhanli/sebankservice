package entity;

import entity.HesapHareketi;
import entity.Kart;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-06-15T05:03:06")
@StaticMetamodel(Hesap.class)
public class Hesap_ { 

    public static volatile SingularAttribute<Hesap, String> sonEklenenFaizTarihi;
    public static volatile SingularAttribute<Hesap, Long> hesapId;
    public static volatile SingularAttribute<Hesap, String> bankaAdi;
    public static volatile SingularAttribute<Hesap, Integer> hesapLimiti;
    public static volatile ListAttribute<Hesap, HesapHareketi> hesapHareketListesi;
    public static volatile SingularAttribute<Hesap, String> hesapFullAd;
    public static volatile SingularAttribute<Hesap, Long> hesapNo;
    public static volatile SingularAttribute<Hesap, Kart> kart;
    public static volatile SingularAttribute<Hesap, String> hesapTipi;
    public static volatile SingularAttribute<Hesap, Integer> hesapBakiye;
    public static volatile SingularAttribute<Hesap, Double> hesapFaizOrani;

}