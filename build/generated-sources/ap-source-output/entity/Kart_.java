package entity;

import entity.Hesap;
import entity.Musteri;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-06-15T05:03:06")
@StaticMetamodel(Kart.class)
public class Kart_ { 

    public static volatile SingularAttribute<Kart, Long> kartId;
    public static volatile SingularAttribute<Kart, Long> kartNumarasi;
    public static volatile ListAttribute<Kart, Hesap> hesapList;
    public static volatile SingularAttribute<Kart, Musteri> musteri;

}