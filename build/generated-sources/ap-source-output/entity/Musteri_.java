package entity;

import entity.Kart;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-06-15T05:03:06")
@StaticMetamodel(Musteri.class)
public class Musteri_ { 

    public static volatile SingularAttribute<Musteri, String> musteriSoyadi;
    public static volatile SingularAttribute<Musteri, String> musteriAdi;
    public static volatile SingularAttribute<Musteri, String> sifre;
    public static volatile SingularAttribute<Musteri, String> musteriTcNo;
    public static volatile SingularAttribute<Musteri, String> cinsiyet;
    public static volatile SingularAttribute<Musteri, String> musteriKayitTarihi;
    public static volatile SingularAttribute<Musteri, Long> musteriId;
    public static volatile ListAttribute<Musteri, Kart> kartList;

}